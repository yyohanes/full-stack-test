## Frontend Test Project

This test is divided into *ONE* mandatory challenge and 5 optional challenges.

In order to submit the project, you MUST complete *Challenge 1*
For every optional challenge completed, you will earn score that will be summed into your final score.

We will select candidates who gets the highest score
_(You do not need to complete all)_

### Materials
1. Please find the mockup design in `mockup` folder
2. Please consume Products resource from https://kommercio.id/full-stack-test/products.json

### Submission
Submission can be done in TWO ways:

1. Send zipped project files to yohanes@kommercio.id
2. **EXTRA 50 POINT**: Upload it to Github and email Github link to yohanes@kommercio.id

---

## Challenge 1 (MANDATORY)
**Build the mockup website using Vue.js**

---

### OPTIONAL CHALLENGES

### Challenge 2 (Max score: 50)
Use **Laravel** to build this project

### Challenge 3 (Max score: 50)
Use **SASS** or **Less** CSS pre-processors

### Challenge 4 (Max score: 100)
Build this project with **Webpack** OR **Laravel Mix (If you do challenge 2)**.

### Challenge 5 (Max score: 100)
Add unit-testing

### Challenge 6 (Max score: 50)
Submit your project on Github
